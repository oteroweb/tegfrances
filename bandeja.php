<?php 
// proceso de conexión con la base de datos
include('conexionbd.php');

// iniciar sesion
session_start();

// validar si se esta ingresando con sesión correctamente
if (!$_SESSION) {
  header("location:index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Apprendre le Français</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bandeja.css">
	  <!-- Librería jQuery requerida por los plugins de JavaScript -->
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-md-12 fondo">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6"><b>Heure Française </b>
						<span id="contenedor">
<script>
<!--
function mostrarclock(){
if (!document.all&&!document.getElementById)
return
thelement=document.getElementById? document.getElementById("contenedor"): document.all.contendor
var Digital=new Date()
var hours=Digital.getHours()
var minutes=Digital.getMinutes()
var seconds=Digital.getSeconds()
var dn="PM"
if (hours<12)
dn="AM"
if (hours>12)
hours=hours-12
if (hours==0)
hours=12
if (minutes<=9)
minutes="0"+minutes
if (seconds<=9)
seconds="0"+seconds
var ctime=hours+":"+minutes+":"+seconds+" "+dn
thelement.innerHTML="<b style='font-size:15;color:cyan;'>"+ctime+"</b>"
setTimeout("mostrarclock()",1000)
}
window.onload=mostrarclock
//-->
</script>
</span>
					</div>
					<div class="col-sm-6 col-md-6 textoaladerecha">
						<a href="index.php" class="desconectar">Disconnect</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="container-fluid">
	<div class="row">
		<hr>
	</div>
</div>

<div class="container-fluid">
	<div class="row titulo">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					Première Leçon
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="col-sm-2 col-md-2 pading">
				<div class="col-sm-12 col-md-12 textocentrado">
					<b>Verbes</b>
				</div>
				<div class="col-sm-12 col-md-12 fondoimagen">
					<a href="verbos.php"><img src="img/ejeruno.jpg" class="img-responsive imagen"></a>
				</div>
				</div>
				<div class="col-sm-2 col-md-2 pading">
					<div class="col-md-12 textocentrado">
						<b>Pluriels</b>
					</div>
					<div class="col-sm-12 col-md-12 fondoimagen">
						<a href="plural.php"><img src="img/ejerdos.jpg" class="img-responsive imagen"></a>
					</div>
				</div>
				<div class="col-sm-2 col-md-2 pading">
					<div class="col-md-12 textocentrado">
						<b>Expressions</b>
					</div>
					<div class="col-sm-12 col-md-12 fondoimagen">
						<a href="expresion.php"><img src="img/ejertres.jpg" class="img-responsive imagen"></a>
					</div>
				</div>
				<div class="col-sm-2 col-md-2 pading">
					<div class="col-md-12 textocentrado">
						<b>Aliments</b>
					</div>
					<div class="col-sm-12 col-md-12 fondoimagen">
						<a href="alimento.php"><img src="img/ejercuatro.jpg" class="img-responsive imagen"></a>
					</div>
				</div>
				<div class="col-sm-2 col-md-2 pading">
					<div class="col-md-12 textocentrado">
						<b>Animaux</b>
					</div>
					<div class="col-sm-12 col-md-12 fondoimagen">
						<a href="animal.php"><img src="img/ejercinco.jpg" class="img-responsive imagen"></a>
					</div>
				</div>
				<div class="col-sm-2 col-md-2 pading">
					<div class="col-sm-12 col-md-12 textocentrado">
						<b>Évaluation</b>
					</div>
					<div class="col-sm-12 col-md-12 fondoimagen">
						<a href="prueba.php"><img src="img/ejerseis.jpg" class="img-responsive imagen"></a>
					</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid">
	<div class="row">
		<hr>
	</div>
</div>

<div class="container-fluid">
	<div class="row titulo">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					Leçon Audiovisuel
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-3 col-md-2 pading">
			<div class="col-sm-12 col-md-12 textocentrado">
				<b>Introduction</b>
			</div>
			<div class="col-sm-12 col-md-12 fondoimagen">
				<a href="introduccionuno.php"><img src="img/introduccion.jpg" class="img-responsive imagen"></a>
			</div>
		</div>
		<div class="col-sm-3 col-md-2 pading">
			<div class="col-sm-12 col-md-12 textocentrado">
				<b>Leçon de base</b>
			</div>
			<div class="col-sm-12 col-md-12 fondoimagen">
				<a href="introducciondos.php"><img src="img/basico.jpg" class="img-responsive imagen"></a>
			</div>
		</div>
		<div class="col-sm-3 col-md-2 pading">
			<div class="col-sm-12 col-md-12 textocentrado">
				<b>Leçon avancée</b>
			</div>
			<div class="col-sm-12 col-md-12 fondoimagen">
				<a href="introducciontres.php"><img src="img/medio.jpg" class="img-responsive imagen"></a>
			</div>
		</div>
		<div class="col-sm-3 col-md-2 pading">
			<div class="col-sm-12 col-md-12 textocentrado">
				<b>Leçon Dernier</b>
			</div>
			<div class="col-sm-12 col-md-12 fondoimagen">
				<a href="introduccioncuatro.php"><img src="img/pasado.jpg" class="img-responsive imagen"></a>
			</div>
		</div>
		<div class="col-sm-3 col-md-2 pading">
			<div class="col-sm-12 col-md-12 textocentrado">
				<b>Leçon Présent</b>
			</div>
			<div class="col-sm-12 col-md-12 fondoimagen">
				<a href="introduccioncinco.php"><img src="img/presente.jpg" class="img-responsive imagen"></a>
			</div>
		</div>
		<div class="col-sm-3 col-md-2 pading">
			<div class="col-sm-12 col-md-12 textocentrado">
				<b>Leçon Avenir</b>
			</div>
			<div class="col-sm-12 col-md-12 fondoimagen">
				<a href="introduccionseis.php"><img src="img/futuro.jpg" class="img-responsive imagen"></a>
			</div>
		</div>

	</div>
</div>


<div class="container-fluid">
	<div class="row">
		<hr>
	</div>
</div>

<div class="container-fluid">
	<div class="row titulo">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					Options Supplémentaires
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<!-- <div class="col-sm-4 col-md-2 col-md-offset-3 pading">
			<div class="col-sm-12 col-md-12 textocentrado">
				<b>TV en ligne</b>
			</div>
			<div class="col-sm-12 col-md-12 fondoimagen">
				<a href="http://pluzz.francetv.fr/videos/jt_1920_paris_ile_de_france_,126848232.html" target="_blank"><img src="img/tv.jpg" class="img-responsive imagen"></a>
			</div>
		</div> -->
		<div class="col-sm-4 col-md-offset-2 col-md-2 pading">
			<div class="col-md-12 textocentrado">
				<b>Bibliothèque</b>
			</div>
			<div class="col-sm-12 col-md-12 fondoimagen">
				<a href="biblioteque.php"><img src="img/biblio.jpg" class="img-responsive imagen"></a>
			</div>
		</div>
		<div class="col-sm-4 col-md-offset-4 col-md-2 pading">
			<div class="col-md-12 textocentrado">
				<b>Musique</b>
			</div>
			<div class="col-sm-12 col-md-12 fondoimagen">
				<a href="musique.php"><img src="img/musica.jpg" class="img-responsive imagen"></a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<hr>
	</div>
</div>

</body>
</html>