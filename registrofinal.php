<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Apprendre le Français</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/registro.css">
</head>
<body>



<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-md-12 fondo">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						Apprendre le Français
					</div>
					<div class="col-sm-6 col-md-6">
						<form class="form-horizontal">
							<div class="col-sm-5 col-md-5">
								<div class="form-group">
							      <div class="col-sm-12 col-md-12">
							        <input type="text" class="form-control" id="inputEmail" placeholder="utilisateur" name="usuario">
							      </div>
							    </div>
							</div>
							<div class="col-sm-5 col-md-5">
								<div class="form-group">
							      <div class="col-sm-12 col-md-12">
							        <input type="password" class="form-control" id="inputPassword" placeholder="mot de passe" name="contrasena">
							      </div>
							    </div>
							</div>
							<div class="col-sm-2 col-md-2">
								<div class="form-group">
							      <div class="col-sm-12 col-md-12">
							        <button type="submit" class="btn btn-primary">Relier</button>
							      </div>
							    </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<hr>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12 textocentrado final">
			Votre inscription a été complété , vous pouvez vous connecter <br><br><a href="index.php">INDEX</a>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<hr>
	</div>
</div>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12 textocentrado">
				Tous Droits Réservés
			</div>
		</div>
	</div>
</footer>

</body>
</html>