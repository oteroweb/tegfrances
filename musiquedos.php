<?php 
// proceso de conexión con la base de datos
include('conexionbd.php');

// iniciar sesion
session_start();

// validar si se esta ingresando con sesión correctamente
if (!$_SESSION) {
  header("location:index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Apprendre le Français</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/musique.css">
	  <!-- Librería jQuery requerida por los plugins de JavaScript -->
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-md-12 fondo">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<a href="bandeja.php" class="boton">inicio</a>
					</div>
					<div class="col-sm-6 col-md-6 textoaladerecha">
						<a href="index.php" class="desconectar">Disconnect</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 well">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<font size="5">Musique</font>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-6 margensuperior textocentrado">
			<b>Artiste: Indilla / Thème: Ego</b>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6 margensuperior textocentrado">
			<b>Artiste: Indilla / Thème: Run Run</b>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6 margensuperior textocentrado">
			<video src="video/6.mp4" width="500" controls class="contenedor"></video>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6 margensuperior textocentrado">
			<video src="video/7.mp4" width="500" controls class="contenedor"></video>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-6 margensuperior textocentrado">
			<b>Artiste: Indilla / Thème: Clip</b>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6 margensuperior textocentrado">
			<b>Artiste: Indilla / Thème: Interview Vevo</b>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6 margensuperior textocentrado">
			<video src="video/8.mp4" width="500" controls class="contenedor"></video>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6 margensuperior textocentrado">
			<video src="video/10.mp4" width="500" controls class="contenedor"></video>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12 textocentrado">
			<ul class="pagination pagination-lg">
			  <li class="disabled"><a href="#">«</a></li>
			  <li><a href="musique.php">1</a></li>
			  <li class="active"><a href="musiquedos.php">2</a></li>
			  <li><a href="#">»</a></li>
			</ul>
		</div>
	</div>
</div>

</body>
</html>