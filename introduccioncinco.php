<?php 
// proceso de conexión con la base de datos
include('conexionbd.php');

// iniciar sesion
session_start();

// validar si se esta ingresando con sesión correctamente
if (!$_SESSION) {
  header("location:index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Apprendre le Français</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/ejercicio.css">
	  <!-- Librería jQuery requerida por los plugins de JavaScript -->
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-md-12 fondo">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<a href="bandeja.php" class="boton">inicio</a>
					</div>
					<div class="col-sm-6 col-md-6 textoaladerecha">
						<a href="index.php" class="desconectar">Disconnect</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 well">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<font size="5">Introduction à la française</font>
					</div>	
				</div>
			</div>
		</div>	
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<hr>		
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12 textocentrado">
			<font size="5"><b>Cinquième leçon audiovisuel</b></font>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 well textocentrado margensuperior">
			<video src="video/leccion5.webm" width="500" controls class="contenedor"></video>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<ul class="pager">
  <li class="previous"><a href="#">← <b>précédent</b></a></li>
  <li class="next"><a href="#"><b>prochaine</b> →</a></li>
</ul>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<hr>		
	</div>
</div>

</body>
</html>
