<?php 
// proceso de conexión con la base de datos
include('conexionbd.php');

// iniciar sesion
session_start();

// validar si se esta ingresando con sesión correctamente
if (!$_SESSION) {
  header("location:index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Apprendre le Français</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/ejercicio.css">
	  <!-- Librería jQuery requerida por los plugins de JavaScript -->
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-md-12 fondo">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<a href="bandeja.php" class="boton">inicio</a>
					</div>
					<div class="col-sm-6 col-md-6 textoaladerecha">
						<a href="index.php" class="desconectar">Disconnect</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<hr>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4">
			<img src="img/fseis.jpg" class="img-responsive borderradius">
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 well margensuperior">
			<div class="container">
				<div class="row">
					<div class="col-md-12 textocentrado">
						<a class="traductor" data-toggle="tooltip" title="los jovenes">jeune</a>
					</div>
					<div class="col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4">
						<form class="form-horizontal" method="POST" action="plural.php">
							<div class="form-group">
						      <div class="col-md-12">
						        <input type="text" class="form-control" id="inputEmail" name="manger">
						      </div>
						    </div>
						    <div class="form-group">
						      <div class="col-md-12 textocentrado">
						        <button type="submit" class="btn btn-primary">Corriger</button>
						      </div>
						    </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12 textocentrado">
			<?php
						$manger = isset($_POST['manger']) ? $_POST['manger'] : '';

					if (isset($manger) and $manger!="") {
						if ($manger=="los jovenes") {
							echo '<font size="4" color="black"><b>Droite.</b></font><br>';
							echo '<a href="pluraldos.php" class="botonas">Suivant</a>';
						}  else {
							echo '<font size="4" color="#FA8100"><b>Incorrect</b></font><br>';
						}
					}
					
				?>
		</div>
	</div>
</div>


</body>
</html>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>