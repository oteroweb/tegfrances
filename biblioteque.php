<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Apprendre le Français</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bandeja.css">
	  <!-- Librería jQuery requerida por los plugins de JavaScript -->
	<script src="http://code.jquery.com/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-md-12 fondo">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<a href="bandeja.php" class="boton">inicio</a>
					</div>
					<div class="col-sm-6 col-md-6 textoaladerecha">
						<a href="index.php" class="desconectar">Disconnect</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12 well">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<font size="5">Bibliothèque</font>
					</div>	
				</div>
			</div>
		</div>	
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<hr>
	</div>
</div>

<div class="container">
	<div class="row well">
		<div class="col-md-4 col-sm-12">
			
			<div class="col-md-12 textocentrado">
				<img src="img/dunes.jpg">		
			</div>

			<div class="col-md-12 textocentrado"><br>
				<font size="3"><b>Alexandre Dumas</b></font><br>			
			</div>

			<div class="col-md-12 textocentrado"><br>
				<a href="libros/montecristo1.pdf" download="">LE COMTE DE MONTE CRISTO 1</a><br>
				<a href="libros/montecristo2.pdf" download="">LE COMTE DE MONTE CRISTO 2</a><br>
				<a href="libros/montecristo3.pdf" download="">LE COMTE DE MONTE CRISTO 3</a><br>
				<a href="libros/montecristo4.pdf" download="">LE COMTE DE MONTE CRISTO 4</a><br>
			</div>

		</div>

		<div class="col-md-4 col-sm-12">
			
			<div class="col-md-12 textocentrado">
				<img src="img/emile.jpg">		
			</div>

			<div class="col-md-12 textocentrado"><br>
				<font size="3"><b>Emile Zola</b></font><br>			
			</div>

			<div class="col-md-12 textocentrado"><br>
				<a href="libros/emile.pdf" download="">GERMINAL</a><br>				
			</div>

		</div>

		<div class="col-md-4 col-sm-12">
			
			<div class="col-md-12 textocentrado">
				<img src="img/victorhugo.jpg">		
			</div>

			<div class="col-md-12 textocentrado"><br>
				<font size="3"><b>Victor Hugo</b></font><br>			
			</div>

			<div class="col-md-12 textocentrado"><br>
				<a href="libros/hugo1.pdf" download="">LES MISÉRABLES</a><br>
				<a href="libros/hugo1.pdf" download="">NOTRE-DAME DE PARIS</a><br>
				
				
			</div>

		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<br><br>
	</div>
</div>

<div class="container">
	<div class="row well">
		<div class="col-md-4 col-sm-12">
			
			<div class="col-md-12 textocentrado">
				<img src="img/Maupassant.jpg">		
			</div>

			<div class="col-md-12 textocentrado"><br>
				<font size="3"><b>Maupassant</b></font><br>			
			</div>

			<div class="col-md-12 textocentrado"><br>
				<a href="libros/maupassant1.pdf" download="">LE HORLA</a><br>
			</div>

		</div>

		<div class="col-md-4 col-sm-12">
			
			<div class="col-md-12 textocentrado">
				<img src="img/julesverne.jpg">		
			</div>

			<div class="col-md-12 textocentrado"><br>
				<font size="3"><b>Jules Verne</b></font><br>			
			</div>

			<div class="col-md-12 textocentrado"><br>
				<a href="libros/julesverne1.pdf" download="">LE TOUR DU MONDE EN 80 JOURS</a><br>				
			</div>

		</div>

		<div class="col-md-4 col-sm-12">
			
			<div class="col-md-12 textocentrado">
				<img src="img/shakespeare.jpg">		
			</div>

			<div class="col-md-12 textocentrado"><br>
				<font size="3"><b>Shakespeare</b></font><br>			
			</div>

			<div class="col-md-12 textocentrado"><br>
				<i>Traduit</i><br><a href="libros/hugo1.pdf" download="">HANLET</a><br>	
				
			</div>

		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<hr>
	</div>
</div>

</body>
</html>