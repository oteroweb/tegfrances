<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Apprendre le Français</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/programa.css">
</head>
<body>



<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-md-12 fondo">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						Apprendre le Français
					</div>
					<div class="col-sm-6 col-md-6">
						<form class="form-horizontal" method="POST" action="verificando.php">
							<div class="col-sm-5 col-md-5">
								<div class="form-group">
							      <div class="col-sm-12 col-md-12">
							        <input type="text" class="form-control" id="inputEmail" placeholder="utilisateur" name="usuario">
							      </div>
							    </div>
							</div>
							<div class="col-sm-5 col-md-5">
								<div class="form-group">
							      <div class="col-sm-12 col-md-12">
							        <input type="password" class="form-control" id="inputPassword" placeholder="mot de passe" name="contrasena">
							      </div>
							    </div>
							</div>
							<div class="col-sm-2 col-md-2">
								<div class="form-group">
							      <div class="col-sm-12 col-md-12">
							        <button type="submit" class="btn btn-primary">Relier</button>
							      </div>
							    </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-5 col-md-4 fondodos">
			<form class="form-horizontal" method="POST" action="registro.php">
			  <fieldset>
			    <div class="form-group">
			      <div class="col-md-12">
			        <input type="text" class="form-control" id="inputEmail" placeholder="utilisateur" name="usuario">
			      </div>
			    </div>

			    <div class="form-group">
			      <div class="col-md-12">
			        <input type="password" class="form-control" id="inputPassword" placeholder="mot de passe" name="contrasena">
			      </div>
			    </div>

			    <div class="form-group">
			      <div class="col-md-12">
			        <button type="submit" class="btn btn-primary">Record</button>
			      </div>
			    </div>
			  </fieldset>
			</form>
		</div>
	</div>
</div>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12 textocentrado">
				Tous Droits Réservés
			</div>
		</div>
	</div>
</footer>

</body>
</html>