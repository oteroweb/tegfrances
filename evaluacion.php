<?php 
// proceso de conexión con la base de datos
include('conexionbd.php');

// iniciar sesion
session_start();

// validar si se esta ingresando con sesión correctamente
if (!$_SESSION) {
  header("location:index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Apprendre le Français</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bandeja.css">
</head>
<body>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-md-12 fondo">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<a href="bandeja.php" class="boton">inicio</a>
					</div>
					<div class="col-sm-6 col-md-6 textoaladerecha">
						<a href="index.php" class="desconectar">Disconnect</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<hr>
	</div>
</div>

<div class="container">
	<div class="row">
		<div id="timer" class="col-md-3 col-md-offset-4 textocentrado well">
			25 temps restant
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-3 col-md-offset-4 textocentrado well">
			<img src="img/ejercuatro.jpg">
		</div>
	</div>
</div>

	
<div class="container">
	<div class="row">
		<div class="col-md-3 col-md-offset-4 textocentrado">
			<form method="post" action="evaluacion.php">
				<div class="form-group">
			      <div id="respuesta" class="col-lg-12">
			        	<input type="text" name='respuesta' class="form-control" id="inputPassword"><br>
			        	<button type="submit" id="boton" class="btn btn-primary">accepter</button>
			        	
			      </div> 
			        	<?php 
if (isset($_POST['respuesta'])) {

	if ($_POST['respuesta']=='aliments') {echo "<p style='color:green'>bonne réponse </p> <a href='evaluacion2.php'> l'année prochaine</a>";}

	else echo "<p style='color:orange'>mauvaise réponse </p> ";   
	

}
	else {
?> 
<script type="text/javascript">
var myCounter = new Countdown({  
    seconds:25,  // number of seconds to count down
    onUpdateStatus: function(sec){

    document.getElementById("timer").innerHTML=sec + " temps restant";}, // callback for each second
    onCounterEnd: function(){ alert('vous avez manquez de temps');
//document.getElementById("boton").disabled = true; 
document.getElementById("respuesta").innerHTML="<a href='bandeja.php'>bac d' utilisateur</a>";
    } // final action
});
 // watch for spelling

myCounter.start();


function Countdown(options) {
  var timer,
  instance = this,
  seconds = options.seconds || 10,
  updateStatus = options.onUpdateStatus || function () {},
  counterEnd = options.onCounterEnd || function () {};

  function decrementCounter() {
    updateStatus(seconds);
    if (seconds === 0) {
      counterEnd();
      instance.stop();
    }
    seconds--;
  }

  this.start = function () {
    clearInterval(timer);
    timer = 0;
    seconds = options.seconds;
    timer = setInterval(decrementCounter, 1000);
  };

  this.stop = function () {
    clearInterval(timer);
  };
}


</script>
<?php 
	 }
 ?>
			    </div>
			</form>
		</div>
	</div>
</div>
</form>



</body>
</html>